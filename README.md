# FCC Heat Map

[Live Demo](http://feeble-base.surge.sh/)

## Description
This project utilizes d3.js to display a heat map that visualizes monthly variance of Global Land-Surface Temperatures (in Celsisus) over the years.

When the user places the cursor over one of the cells of the map, a tooltip will be displayed and provide additional information.

## Uses
* HTML
* CSS
* JavaScript
* D3.js

## About
This project was written as a requirement in the [Data Visualization Projects](https://www.freecodecamp.org/challenges/visualize-data-with-a-heat-map)
 for [FreeCodeCamp](https://www.freecodecamp.org)


## License
[MIT](https://tldrlegal.com/license/mit-license) &copy; 2018 [Jennifer Currie](https://github.com/Renestl)