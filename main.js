document.addEventListener("DOMContentLoaded", function(event) {

	var url = "https://raw.githubusercontent.com/FreeCodeCamp/ProjectReferenceData/master/global-temperature.json";

	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	var colors = ["#33a02c", "#e31a1c", "#fb9a99", "#1f78b4", "#a6cee3","#ffff99",  "#6a3d9a", "#cab2d6","#ff7f00","#fdbf6f", "#b2df8a"];

	var legendDivisions = colors.length;
	var legendWidth = 40;

	var margin = {top: 20, right: 10, bottom: 160, left: 70},
				fullWidth = 900,
				fullHeight = 860;

	// used for ranges of the scales
	var width = fullWidth - margin.right - margin.left;
	var height = fullHeight - margin.top - margin.bottom;

	var boxWidth = 50;
	var boxHeight = 50;

	fetch(url)
		.then(function(response) {
			return response.json();
		})
		.then(function(data) {

			var baseTemperature = data.baseTemperature;
			var monthlyVariance = data.monthlyVariance;

			// map year, month, variance data
			var yearData = data.monthlyVariance.map(obj => {
				return obj.year;
			})
			var monthData = data.monthlyVariance.map(obj => {
				return obj.month;
			})
			var varianceData = data.monthlyVariance.map(obj => {
				return obj.variance;
			})

			var lowVariance = d3.min(varianceData);
			var highVariance = d3.max(varianceData);

			var lowYear = d3.min(yearData);
			var highYear = d3.max(yearData);

			var minDate = new Date(lowYear, 0);
			var maxDate = new Date(highYear, 0);

			var gridWidth = width / (yearData.length/12);
			var gridHeight = height / months.length;

			// Scales
			var xScale = d3.scaleTime()
				.domain([minDate, maxDate])
				.range([0, width]);

			var yScale = d3.scaleLinear()
				.domain([1, 12])
				.range([height, 0]);

			var colorScale = d3.scaleQuantile()
				.domain([lowVariance + baseTemperature, highVariance + baseTemperature])
				.range(colors);

			// Axes
			var xAxis = d3.axisBottom()
				.scale(xScale)
				.ticks(20);

			var yAxis = d3.axisLeft()
				.scale(yScale)
				.ticks("")

			// Tooltip
			var tooltip = d3.select("body")
				.append("div")
				.attr("class", "tooltip");

			// Chart
			var chart = d3.select(".chart")
				.attr("width", fullWidth)
				.attr("height", fullHeight)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// Add axes to chart
			chart.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + (height + 1) + ")")
				.call(xAxis);
			
			chart.append("g")
				.attr("class", "y axis")
				.attr("style", "padding")
				.call(yAxis);

			// text label for axes
			chart.append("text")
				.attr("transform", "translate(" + (width/2) + "," + (height + margin.top + 30) +")")
				.text("Years");

			chart.append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 0 - margin.left)
				.attr("x", 0 - height/2)
				.attr("dy", "1em")
				.style("text-anchor", "middle")
				.text("Months");	

			var monthLabels = chart.selectAll(".monthLabel")
				.data(months)
				.enter()
				.append("text")
				.text(function(d) {
					return d;
				})
				.attr("x", 0)
				.attr("y", function(d,i) {
					return i * gridHeight;
				})
				.style("text-anchor", "end")
				.attr("transform", "translate(-6," + gridHeight / 1.5 + ")")
				.attr("class", "monthLabel scales axis axis-months");


			// Add chart data
			chart.selectAll(".bar")
				.data(monthlyVariance, function(d) {
					return (d.year + ":" + d.month);
				})
				.enter()
					.append("rect")
					.attr("class", "bar")
				// position on x,y axes
				.attr("x", function(d) { return ((d.year - lowYear) * gridWidth); })
				.attr("y", function(d) { return ((d.month - 1) * gridHeight); })
				// height and width of data boxes
				.attr("width", gridWidth)
				.attr("height", gridHeight)
				.style("fill", function(d) {
					return colorScale(d.variance + baseTemperature);
				})
				.on("mouseover", function(d) {
					tooltip.transition()
						.duration(200)
						.style("opacity", 1);

					tooltip.html(`<strong> ${d.year}: ${months[d.month-1]} <br />
						Temp: ${baseTemperature + d.variance}</strong><br />
						Variance: ${d.variance}<br />
					`)
						.style("left", (d3.event.pageX) - 50 + "px")
						.style("top", (d3.event.pageY) - 60 + "px");
					})
				.on("mouseout", function(d) {
					tooltip.transition()
						.duration(500)
						.style("opacity", 0);
	
					});

				// Legend
				var legend = chart.selectAll(".legend")
					.data([0].concat(colorScale.quantiles()), function(d) {
					return d;
				})
				.enter()
				.append("g")
					.attr("class", "legend")
				
				legend.append("rect")
					.attr("x", function(d, i) {
						return legendWidth * i + (width - legendWidth * legendDivisions);
					})
					.attr("y", height + 70)
					.attr("width", legendWidth)
					.attr("height", gridHeight / 3)
					.style("fill", function(d, i) {
						return colors[i];
					});

				legend
				.append("text")
				.attr("class", "scales")
				.text(function(d) {
					return (Math.floor(d * 10) / 10);
				})
				.attr("x", function(d, i) {
					return ((legendWidth * i) + Math.floor(legendWidth / 2) - 10  + (width - legendWidth * legendDivisions));
				})
				.attr("y", height + 115);
		});

// end document.addEventListener
});